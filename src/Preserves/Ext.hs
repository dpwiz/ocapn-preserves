{-# OPTIONS_GHC -Wno-partial-type-signatures #-}
{-# OPTIONS_GHC -Wno-orphans #-}

{-# LANGUAGE PartialTypeSignatures #-}

module Preserves.Ext
  ( module Preserves
  , module Preserves.Ext
  ) where

import Preserves

import Data.ByteString.Lazy qualified as BSL
import Data.Text.Lazy qualified as LT
import Data.Void (absurd)

decodeError :: Either _decodeError a
decodeError = do
  oops <- fromValue @Atom (Embedded absurd)
  oops `seq` error "DecodeError"

-- * Orphans

instance (FromValue a e, FromValue b e) => FromValue (Either a b) e where
  fromValue v =
    case fromValue v of
      Right a ->
        pure $ Left a
      Left _err ->
        fmap Right $ fromValue v

instance (ToValue a e, ToValue b e) => ToValue (Either a b) e where
  toValue = either toValue toValue

-- * Flat patterns

pattern ABool :: Bool -> Value e
pattern ABool b = Atom (Bool b)

pattern AFloat :: Float -> Value e
pattern AFloat n = Atom (Float n)

pattern ADouble :: Double -> Value e
pattern ADouble n = Atom (Double n)

pattern AInteger :: Integer -> Value e
pattern AInteger n = Atom (SignedInteger n)

pattern AString :: LT.Text -> Value e
pattern AString t = Atom (String t)

pattern ASymbol :: LT.Text -> Value e
pattern ASymbol t = Atom (Symbol t)

pattern ABytes :: BSL.ByteString -> Value e
pattern ABytes t = Atom (ByteString t)

pattern CRecord :: Anno e -> [Anno e] -> Value e
pattern CRecord l as = Compound (Record l as)

pattern CSequence :: [Anno e] -> Value e
pattern CSequence vs = Compound (Sequence vs)

-- * Values with empty annotations

pattern ABool_ :: Bool -> Anno e
pattern ABool_ b = NoAnn (ABool b)

pattern AFloat_ :: Float -> Anno e
pattern AFloat_ n = NoAnn (AFloat n)

pattern ADouble_ :: Double -> Anno e
pattern ADouble_ n = NoAnn (ADouble n)

pattern AInteger_ :: Integer -> Anno e
pattern AInteger_ n = NoAnn (AInteger n)

pattern NoAnn :: Value e -> Anno e
pattern NoAnn v = Anno [] v

pattern AString_ :: LT.Text -> Anno e
pattern AString_ t = NoAnn (AString t)

pattern ASymbol_ :: LT.Text -> Anno e
pattern ASymbol_ t = NoAnn (ASymbol t)

pattern ABytes_ :: BSL.ByteString -> Anno e
pattern ABytes_ t = NoAnn (ABytes t)

pattern CRecord_ :: Value e -> [Anno e] -> Anno e
pattern CRecord_ l as = NoAnn (CRecord (NoAnn l) as)

pattern CSequence_ :: [Anno e] -> Anno e
pattern CSequence_ as = NoAnn (CSequence as)

toValue_ :: ToValue a e => a -> Anno e
toValue_ = NoAnn . toValue

fromValue_ :: FromValue a e => Anno e -> Either _decodeError a
fromValue_ = \case
  NoAnn v ->
    fromValue v
  _unexpectedAnnotation ->
    decodeError

-- * S-expressions

pattern SExp :: LT.Text -> [Anno e] -> Value e
pattern SExp label args = CSequence (ASymbol_ label : args)

pattern SExp2 :: LT.Text -> Anno e -> Value e
pattern SExp2 k v = CSequence [ASymbol_ k, v]

pattern SExp_ :: LT.Text -> [Anno e] -> Anno e
pattern SExp_ label args = NoAnn (SExp label args)

pattern SExp2_ :: LT.Text -> Anno e -> Anno e
pattern SExp2_ k v = NoAnn (SExp2 k v)
