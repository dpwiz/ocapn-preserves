module Data.OCapN.Signature
  ( Signature(..)
  ) where

import Data.ByteString.Lazy qualified as BSL
import GHC.Generics (Generic)
import Preserves.Ext qualified as Preserves
import Preserves.Ext (pattern SExp_, pattern SExp2, pattern SExp2_)

data Signature =
  EdDSA
    { r :: BSL.ByteString
    , s :: BSL.ByteString
    }
  deriving stock (Eq, Show, Generic)

pattern SigVal :: Preserves.Anno e -> Preserves.Value e
pattern SigVal s =
  SExp2 "sig-val" s

pattern Eddsa :: BSL.ByteString -> BSL.ByteString -> Preserves.Anno e
pattern Eddsa r s =
  SExp_ "eddsa"
    [ SExp2_ "r" (Preserves.ABytes_ r)
    , SExp2_ "s" (Preserves.ABytes_ s)
    ]

instance Preserves.FromValue Signature () where
  fromValue = \case
    SigVal (Eddsa r s) ->
      pure EdDSA{r, s}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue Signature () where
  toValue = \case
    EdDSA{r, s} ->
      SigVal (Eddsa r s)
