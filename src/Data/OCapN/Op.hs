{-# LANGUAGE DuplicateRecordFields #-}

module Data.OCapN.Op
  ( StartSession(..)
  , Bootstrap(..)
  , Listen(..)
  , DeliverOnly(..)
  , Deliver(..)
  , Pick(..)
  , Abort(..)
  , GcExport(..)
  , GcAnswer(..)

  , pattern Op
  ) where

import Data.Text.Lazy qualified as LT
import GHC.Generics (Generic)
import GHC.Natural (Natural)
import Preserves.Ext qualified as Preserves

import Data.OCapN.Desc qualified as Desc
import Data.OCapN.PublicKey (PublicKey)
import Data.OCapN.Reference (Reference)
import Data.OCapN.Signature (Signature)

-- * Types

-- ** StartSession

data StartSession = StartSession
  { captpVersion          :: LT.Text
  , sessionPubkey         :: PublicKey
  , acceptableLocation    :: Reference
  , acceptableLocationSig :: Signature
  } deriving stock (Eq, Show, Generic)

instance Preserves.ToValue StartSession () where
  toValue StartSession{..} =
    OpStartSession
      captpVersion
      (Preserves.toValue_ sessionPubkey)
      (Preserves.toValue_ acceptableLocation)
      (Preserves.toValue_ acceptableLocationSig)

instance Preserves.FromValue StartSession () where
  fromValue = \case
    OpStartSession captpVersion sessionPubkey' acceptableLocation' acceptableLocationSig' -> do
      sessionPubkey <- Preserves.fromValue_ sessionPubkey'
      acceptableLocation <- Preserves.fromValue_ acceptableLocation'
      acceptableLocationSig <- Preserves.fromValue_ acceptableLocationSig'
      pure StartSession{..}
    _ ->
      Preserves.decodeError

pattern OpStartSession
  :: LT.Text
  -> Preserves.Anno e
  -> Preserves.Anno e
  -> Preserves.Anno e
  -> Preserves.Value e
pattern OpStartSession captpVersion sessionPubkey acceptableLocation acceptableLocationSig =
  Op "op:start-session"
    [ Preserves.AString_ captpVersion
    , sessionPubkey
    , acceptableLocation
    , acceptableLocationSig
    ]

-- ** Bootstrap

data Bootstrap = Bootstrap
  { answerPosition :: Natural
  , resolveMeDesc :: Either Desc.ImportObject Desc.ImportPromise
  }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue Bootstrap a where
  fromValue = \case
    OpBootstrap answerPosition' resolveMeDesc'
      | answerPosition' >= 0 -> do
          let answerPosition = fromInteger answerPosition'
          resolveMeDesc <- Preserves.fromValue_ resolveMeDesc'
          pure Bootstrap{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue Bootstrap a where
  toValue Bootstrap{..} =
    OpBootstrap
      (toInteger answerPosition)
      (Preserves.toValue_ resolveMeDesc)

pattern OpBootstrap
  :: Integer
  -> Preserves.Anno e
  -> Preserves.Value e
pattern OpBootstrap answerPosition resolveMeDesc =
  Op "op:bootstrap"
    [ Preserves.AInteger_ answerPosition
    , resolveMeDesc
    ]

-- ** Listen

data Listen
  = Listen
    { toDesc :: Either Desc.Export Desc.Answer
    , resolveMeDesc :: Desc.ImportObject
    , wantsPartial :: Bool
    }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue Listen () where
  fromValue = \case
    OpListen toDesc' litenDesc' wantsPartial -> do
      toDesc <- Preserves.fromValue_ toDesc'
      resolveMeDesc <- Preserves.fromValue_ litenDesc'
      pure Listen{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue Listen () where
  toValue Listen{..} =
    OpListen
      (Preserves.toValue_ toDesc)
      (Preserves.toValue_ resolveMeDesc)
      wantsPartial

pattern OpListen
  :: Preserves.Anno e
  -> Preserves.Anno e
  -> Bool
  -> Preserves.Value e
pattern OpListen toDescExport listenDesc wantsPartial =
  Op "op:listen"
    [ toDescExport
    , listenDesc
    , Preserves.ABool_ wantsPartial
    ]

-- ** DeliverOnly

data DeliverOnly = DeliverOnly
  { to :: Desc.Export
  , args :: [Preserves.Value ()]
  }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue DeliverOnly () where
  fromValue = \case
    OpDeliverOnly to' args' -> do
      to <- Preserves.fromValue_ to'
      args <- traverse Preserves.fromValue_ args'
      pure DeliverOnly{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue DeliverOnly () where
  toValue DeliverOnly{..} =
    OpDeliverOnly
      (Preserves.toValue_ to)
      (map Preserves.toValue_ args)

pattern OpDeliverOnly
  :: Preserves.Anno e
  -> [Preserves.Anno e]
  -> Preserves.Value e
pattern OpDeliverOnly to args =
  Op "op:deliver-only"
    [ to
    , Preserves.CSequence_ args
    ]

-- ** Deliver

data Deliver
  = DeliverAnswer
      { toAnswer :: Desc.Answer
      , args :: [Preserves.Value ()]
      , resolveMeDesc :: Either Desc.ImportObject Desc.ImportPromise
      }
  | DeliverExport
      { toExport :: Desc.Export
      , args :: [Preserves.Value ()]
      , answerPos :: Natural
      , resolveMeDesc :: Either Desc.ImportObject Desc.ImportPromise
      }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue Deliver () where
  fromValue = \case
    OpDeliverAnswer toAnswer' args' resolveMeDesc' -> do
      toAnswer <- Preserves.fromValue_ toAnswer'
      let args = map Preserves.annoValue args'
      resolveMeDesc <- Preserves.fromValue_ resolveMeDesc'
      pure DeliverAnswer{..}
    OpDeliverExport toExport' args' answerPos' resolveMeDesc'
      | answerPos' >= 0 -> do
          toExport <- Preserves.fromValue_ toExport'
          let args = map Preserves.annoValue args'
          let answerPos = fromInteger answerPos'
          resolveMeDesc <- Preserves.fromValue_ resolveMeDesc'
          pure DeliverExport{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue Deliver () where
  toValue = \case
    DeliverAnswer{..} ->
      OpDeliverAnswer
        (Preserves.toValue_ toAnswer)
        (map Preserves.toValue_ args)
        (Preserves.toValue_ resolveMeDesc)

    DeliverExport{..} ->
      OpDeliverExport
        (Preserves.toValue_ toExport)
        (map Preserves.toValue_ args)
        (toInteger answerPos)
        (Preserves.toValue_ resolveMeDesc)

pattern OpDeliverAnswer
  :: Preserves.Anno e
  -> [Preserves.Anno e]
  -> Preserves.Anno e
  -> Preserves.Value e
pattern OpDeliverAnswer toDescAnswer args resolveMeDesc =
  Op "op:deliver"
    [ toDescAnswer
    , Preserves.CSequence_ args
    , Preserves.ABool_ False
    , resolveMeDesc
    ]

pattern OpDeliverExport
  :: Preserves.Anno e
  -> [Preserves.Anno e]
  -> Integer
  -> Preserves.Anno e
  -> Preserves.Value e
pattern OpDeliverExport toDescExport args answerPos resolveMeDesc =
  Op "op:deliver"
    [ toDescExport
    , Preserves.CSequence_ args
    , Preserves.AInteger_ answerPos
    , resolveMeDesc
    ]

-- ** Pick

data Pick = Pick
  { promisePos :: Either Desc.Answer Desc.ImportPromise
  , selectedValuePos :: Natural
  , newAnswerPos :: Natural
  }
  deriving stock (Eq, Show, Generic)

-- ** Abort

newtype Abort = Abort
  { reason :: LT.Text
  }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue Abort a where
  fromValue = \case
    OpAbort reason ->
      pure Abort{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue Abort a where
  toValue Abort{..} =
    OpAbort reason

pattern OpAbort
  :: LT.Text
  -> Preserves.Value e
pattern OpAbort reason =
  Op "op:abort"
    [ Preserves.AString_ reason
    ]

-- ** GcExport

data GcExport = GcExport
  { exportPosition :: Natural
  , wireDelta :: Natural
  }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue GcExport a where
  fromValue = \case
    OpGcExport exportPosition' wireDelta'
      | exportPosition' >= 0 && wireDelta' >= 0 -> do
          let
            exportPosition = fromInteger exportPosition'
            wireDelta = fromInteger wireDelta'
          pure GcExport{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue GcExport a where
  toValue GcExport{..} =
    OpGcExport
      (toInteger exportPosition)
      (toInteger wireDelta)

pattern OpGcExport
  :: Integer
  -> Integer
  -> Preserves.Value e
pattern OpGcExport exportPosition wireDelta =
  Op "op:gc-export"
    [ Preserves.AInteger_ exportPosition
    , Preserves.AInteger_ wireDelta
    ]

-- ** GcAnswer

newtype GcAnswer = GcAnswer
  { answerPosition :: Natural
  }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue GcAnswer a where
  fromValue = \case
    OpGcAnswer answerPosition'
      | answerPosition' >= 0 -> do
          let answerPosition = fromInteger answerPosition'
          pure GcAnswer{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue GcAnswer a where
  toValue GcAnswer{..} =
    OpGcAnswer
      (toInteger answerPosition)

pattern OpGcAnswer
  :: Integer
  -> Preserves.Value e
pattern OpGcAnswer answerPosition =
  Op "op:gc-answer"
    [ Preserves.AInteger_ answerPosition
    ]

-- * Helpers

pattern Op :: LT.Text -> [Preserves.Anno e] -> Preserves.Value e
pattern Op label args = Preserves.CRecord (Preserves.ASymbol_ label) args
