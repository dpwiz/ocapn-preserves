module Data.OCapN.SwissNum
  ( SwissNum(..)
  ) where

import Data.Text.Lazy qualified as LT
import GHC.Generics (Generic)

newtype SwissNum = SwissNum LT.Text
  deriving stock (Eq, Show, Generic)
