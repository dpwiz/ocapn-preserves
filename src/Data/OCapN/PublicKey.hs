module Data.OCapN.PublicKey
  ( PublicKey(..)
  ) where

import Data.ByteString.Lazy qualified as BSL
import GHC.Generics (Generic)
import Preserves.Ext qualified as Preserves
import Preserves.Ext (pattern SExp_, pattern SExp2, pattern SExp2_)

data PublicKey =
  Ed25519
    { q :: BSL.ByteString
    , s :: BSL.ByteString
    }
  deriving stock (Eq, Show, Generic)

pattern Eddsa25519 :: BSL.ByteString -> BSL.ByteString -> Preserves.Anno e
pattern Eddsa25519 q s =
  SExp_ "ecc"
    [ SExp2_ "curve" (Preserves.ASymbol_ "Ed25519")
    , SExp2_ "flags" (Preserves.ASymbol_ "eddsa")
    , SExp2_ "q" (Preserves.ABytes_ q)
    , SExp2_ "s" (Preserves.ABytes_ s)
    ]

instance Preserves.FromValue PublicKey a where
  fromValue = \case
    SExp2 "public-key" (Eddsa25519 q s) ->
      pure Ed25519{q, s}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue PublicKey a where
  toValue = \case
    Ed25519{q, s} -> SExp2 "public-key" (Eddsa25519 q s)
