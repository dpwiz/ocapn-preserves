module Data.OCapN.SturdyRef
  ( SturdyRef(..)
  ) where

import GHC.Generics (Generic)

import Data.OCapN.Machine (Machine)
import Data.OCapN.SwissNum (SwissNum)

data SturdyRef = SturdyRef
  { machine :: Machine
  , swissNum :: SwissNum
  }
  deriving stock (Eq, Show, Generic)