module Data.OCapN.Machine
  ( Machine(..)
  ) where

import Data.Text.Lazy qualified as LT
import GHC.Generics (Generic)

data Machine = Machine
  { transport :: LT.Text
  , address :: LT.Text
  , hints :: Bool
  }
  deriving stock (Eq, Show, Generic)
