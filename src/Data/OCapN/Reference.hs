module Data.OCapN.Reference
  ( Reference(..)
  ) where

import GHC.Generics (Generic)
import Preserves.Ext qualified as Preserves

newtype Reference = Reference (Preserves.Value ())
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue Reference () where
  fromValue = fmap Reference . Preserves.fromValue

instance Preserves.ToValue Reference () where
  toValue (Reference r) = Preserves.toValue r
