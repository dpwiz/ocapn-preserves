{-# LANGUAGE DuplicateRecordFields #-}

module Data.OCapN.Desc
  ( ImportObject(..)
  , ImportPromise(..)
  , Export(..)
  , Answer(..)
  , SigEnvelope(..)
  , HandoffGive(..)
  , HandoffReceive(..)
  ) where

import Data.ByteString.Lazy qualified as BSL
import Data.Text.Internal.Lazy qualified as LT
import GHC.Generics (Generic)
import GHC.Natural (Natural)
import Preserves.Ext qualified as Preserves

import Data.OCapN.PublicKey (PublicKey)
import Data.OCapN.Reference (Reference)
import Data.OCapN.Signature (Signature)

-- * Types

-- ** ImportObject

newtype ImportObject = ImportObject
  { position :: Natural
  }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue ImportObject a where
  fromValue = \case
    DescImportObject n | n > 0 ->
      pure $ ImportObject (fromInteger n)
    _ ->
      Preserves.decodeError

instance Preserves.ToValue ImportObject a where
  toValue = \case
    ImportObject o ->
      DescImportObject (toInteger o)

pattern DescImportObject :: Integer -> Preserves.Value e
pattern DescImportObject o =
  Desc "desc:import-object"
    [ Preserves.AInteger_ o
    ]

-- ** ImportPromise

newtype ImportPromise = ImportPromise
  { position :: Natural
  }
  deriving stock (Eq, Show, Generic)

instance Preserves.FromValue ImportPromise a where
  fromValue = \case
    DescImportPromise n | n > 0 ->
      pure $ ImportPromise (fromInteger n)
    _ ->
      Preserves.decodeError

instance Preserves.ToValue ImportPromise a where
  toValue = \case
    ImportPromise o ->
      DescImportPromise (toInteger o)

pattern DescImportPromise :: Integer -> Preserves.Value e
pattern DescImportPromise o =
  Desc "desc:import-promise"
    [ Preserves.AInteger_ o
    ]

-- ** Export

newtype Export = Export
  { position :: Natural
  } deriving stock (Eq, Show, Generic)

instance Preserves.FromValue Export a where
  fromValue = \case
    DescExport n | n > 0 ->
      pure $ Export (fromInteger n)
    _ ->
      Preserves.decodeError

instance Preserves.ToValue Export a where
  toValue = \case
    Export o ->
      DescExport (toInteger o)

pattern DescExport :: Integer -> Preserves.Value e
pattern DescExport o =
  Desc "desc:export"
    [ Preserves.AInteger_ o
    ]

-- ** Answer

newtype Answer = Answer
  { position :: Natural
  } deriving stock (Eq, Show, Generic)

instance Preserves.FromValue Answer a where
  fromValue = \case
    DescExport n | n > 0 ->
      pure $ Answer (fromInteger n)
    _ ->
      Preserves.decodeError

instance Preserves.ToValue Answer a where
  toValue = \case
    Answer o ->
      DescAnswer (toInteger o)

pattern DescAnswer :: Integer -> Preserves.Value e
pattern DescAnswer o =
  Desc "desc:answer"
    [ Preserves.AInteger_ o
    ]

-- ** SigEnvelope

data SigEnvelope = SigEnvelope
  { signed    :: Preserves.Value ()
  , signature :: Signature
  } deriving stock (Eq, Show, Generic)

instance Preserves.FromValue SigEnvelope () where
  fromValue = \case
    DescSigEnvelope signed signature' -> do
      signature <- Preserves.fromValue_ signature'
      pure SigEnvelope{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue SigEnvelope () where
  toValue SigEnvelope{..} =
    DescSigEnvelope
      signed
      (Preserves.toValue_ signature)

pattern DescSigEnvelope
  :: Preserves.Value e
  -> Preserves.Anno e
  -> Preserves.Value e
pattern DescSigEnvelope signed signature =
  Desc "desc:sig-envelope"
    [ Preserves.NoAnn signed
    , signature
    ]

-- ** HandoffGive

data HandoffGive = HandoffGive
  { receiverKey      :: PublicKey
  , exporterLocation :: Reference
  , session          :: BSL.ByteString
  , gifterSide       :: PublicKey
  , giftId           :: BSL.ByteString
  } deriving stock (Eq, Show, Generic)

instance Preserves.FromValue HandoffGive () where
  fromValue = \case
    DescHandoffGive receiverKey' exporterLocation' session gifterSide' giftId -> do
      receiverKey <- Preserves.fromValue_ receiverKey'
      exporterLocation <- Preserves.fromValue_ exporterLocation'
      gifterSide <- Preserves.fromValue_ gifterSide'
      pure HandoffGive{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue HandoffGive () where
  toValue HandoffGive{..} =
    DescHandoffGive
      (Preserves.toValue_ receiverKey)
      (Preserves.toValue_ exporterLocation)
      session
      (Preserves.toValue_ gifterSide)
      giftId

pattern DescHandoffGive
  :: Preserves.Anno e
  -> Preserves.Anno e
  -> BSL.ByteString
  -> Preserves.Anno e
  -> BSL.ByteString
  -> Preserves.Value e
pattern DescHandoffGive receiverKey exporterLocation session gifterSide giftId =
  Desc "desc:handoff-give"
    [ receiverKey
    , exporterLocation
    , Preserves.ABytes_ session
    , gifterSide
    , Preserves.ABytes_ giftId
    ]

-- ** HandoffReceive

data HandoffReceive = HandoffReceive
  { receivingSession :: BSL.ByteString
  , receivingSide    :: BSL.ByteString
  , handoffCount     :: Natural
  , signedGive       :: SigEnvelope
  } deriving stock (Eq, Show, Generic)

instance Preserves.FromValue HandoffReceive () where
  fromValue = \case
    DescHandoffReceive receivingSession receivingSide handoffCount' signedGive'
      | handoffCount' > 0 -> do
          let handoffCount = fromInteger handoffCount'
          signedGive <- Preserves.fromValue_ signedGive'
          pure HandoffReceive{..}
    _ ->
      Preserves.decodeError

instance Preserves.ToValue HandoffReceive () where
  toValue HandoffReceive{..} =
    DescHandoffReceive
      receivingSession
      receivingSide
      (toInteger handoffCount)
      (Preserves.toValue_ signedGive)

pattern DescHandoffReceive
  :: BSL.ByteString
  -> BSL.ByteString
  -> Integer
  -> Preserves.Anno e
  -> Preserves.Value e
pattern DescHandoffReceive receivingSession receivingSide handoffCount signedGive =
  Desc "desc:handoff-receive"
    [ Preserves.ABytes_ receivingSession
    , Preserves.ABytes_ receivingSide
    , Preserves.AInteger_ handoffCount
    , signedGive
    ]

-- * Helpers

pattern Desc :: LT.Text -> [Preserves.Anno e] -> Preserves.Value e
pattern Desc label args = Preserves.CRecord (Preserves.ASymbol_ label) args
