# ocapn-preserves

[Preserves] encoding following [spec draft] and [test suite].

[Preserves]: https://preserves.gitlab.io/preserves/preserves-text.html
[spec draft]: https://github.com/ocapn/ocapn/blob/main/draft-specifications/CapTP%20Specification.md
[test suite]: https://github.com/ocapn/ocapn-test-suite
